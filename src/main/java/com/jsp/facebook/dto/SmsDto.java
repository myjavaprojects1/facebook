package com.jsp.facebook.dto;

import java.io.Serializable;

public class SmsDto implements Serializable {

	private String name;
	
	private String contactNumber;
	
	private String smsContent;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getSmsContent() {
		return smsContent;
	}

	public void setSmsContent(String smsContent) {
		this.smsContent = smsContent;
	}
	
	
	
}
