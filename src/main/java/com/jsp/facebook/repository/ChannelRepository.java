package com.jsp.facebook.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jsp.facebook.entity.SmsDetails;



public interface ChannelRepository extends JpaRepository<SmsDetails,Long > {

//	@Query("from SmsDetails where smsStatus=:smsStatus")
//	@Query(value="select * from sms_details where sms_status=:smsStatus",nativeQuery = true)
//	public List<SmsDetails> getSmsByStatus(@Param("smsStatus")String smsStatus);
	
	public List<SmsDetails> findBySmsStatus(String smsStatus); // inbuilt query written based on method name convention 
	
//	@Query("from SmsDetails where name=:name and contactNumber=:contactNumber")
//	public SmsDetails getSmsByNameAndContactNumber(@Param("name") String name,@Param("contactNumber") String ContactNumber);
	
	public SmsDetails findByNameAndContactNumber(String name,String ContactNumber); // inbuilt query written based on method name convention
	
	@Query("select name from SmsDetails")
	public List<String> getAllChannelNames();
	
	@Modifying
	@Transactional
	@Query(value="update sms_details set contact_Number=:contactNumber where alt_Key=:altKey",nativeQuery = true)
	public Integer updateContactNumberByPrimaryKey(@Param("contactNumber") String contactNumber,@Param("altKey") long altKey);
}
