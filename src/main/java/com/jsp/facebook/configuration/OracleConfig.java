package com.jsp.facebook.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Profile("int")
@Configuration
public class OracleConfig {

	public OracleConfig() {
		System.out.println(this.getClass().getSimpleName());
	}
	
	@Bean
	public HikariDataSource getDataSource() {
		HikariConfig config = new HikariConfig();
		config.setUsername("root");
		config.setPassword("root");
		config.setJdbcUrl("jdbc:mysql://localhost:3306/operational_db");
		config.setDriverClassName("com.mysql.cj.jdbc.Driver");
		return new HikariDataSource(config);
	}

}
