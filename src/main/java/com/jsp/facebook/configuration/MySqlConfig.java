package com.jsp.facebook.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Profile("dev")
@Configuration
public class MySqlConfig {
	
	

	public MySqlConfig() {
		System.out.println(this.getClass().getSimpleName());
	}

	@Bean
	public HikariDataSource getDataSource(){
		
		String password = System.getenv("MYSQL_PWD");
		String userName = System.getenv("MYSQL_USERNAME");
		String portNumber = System.getenv("PORT_NUMBER");
		
//		System.out.println(System.getenv("OS"));
//		System.out.println(System.getenv("PATH"));
		
		System.out.println(password);
		System.out.println(userName);
		System.out.println(portNumber);
		
		HikariConfig config = new HikariConfig();
		config.setUsername("root");
		config.setPassword("root");
		config.setJdbcUrl("jdbc:mysql://localhost:3306/operational_db");
		config.setDriverClassName("com.mysql.cj.jdbc.Driver");
		return new HikariDataSource(config);
	}
	
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	public ObjectMapper getObjectMapper() {
		return new ObjectMapper();
	}
	
	@Bean
	public JdbcTemplate getJdbcTemplate(HikariDataSource hikariDataSource) {
		return new JdbcTemplate(hikariDataSource);
		 
	}
}
