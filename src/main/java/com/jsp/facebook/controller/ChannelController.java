package com.jsp.facebook.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jsp.facebook.constants.AppConstants;
import com.jsp.facebook.dto.AppResponseDto;
import com.jsp.facebook.dto.SmsDto;
import com.jsp.facebook.entity.SmsDetails;
import com.jsp.facebook.service.ChannelServiceImpl;

@RestController
public class ChannelController {
	
	
	
	public ChannelController() {
		System.out.println(this.getClass().getSimpleName());
	}

	@Autowired
	private ChannelServiceImpl channelServiceImpl;
	
	Logger logger=LoggerFactory.getLogger(ChannelController.class);
	
	@PostMapping(value=AppConstants.SEND_SMS)
	public @ResponseBody AppResponseDto sendSms(@RequestBody SmsDto smsDto) {
		logger.info("sendSms() method is executed");
		AppResponseDto dto = channelServiceImpl.processSms(smsDto);
		return dto;
	}
	
	@PostMapping(value=AppConstants.SEND_ALL_SMS)
	public @ResponseBody AppResponseDto sendAllSms(@RequestBody List<SmsDto> smsDto) {
		AppResponseDto dto = channelServiceImpl.processAllSms(smsDto);
		return dto;
	}
	
	@GetMapping(value=AppConstants.FIND_SMS_BY_ID)
	public @ResponseBody AppResponseDto findSmsById(@PathVariable("id")  long id) {
		AppResponseDto dto = channelServiceImpl.processFindSmsById(id);
		return dto;
	}
	
	@GetMapping(value=AppConstants.GET_BY_SMS_STATUS)
	public @ResponseBody List<SmsDetails> getBySmsStatus(@RequestParam String smsStatus) {
		 List<SmsDetails> list = channelServiceImpl.processChannelBySmsStatus(smsStatus);
		return list;
	}
	
	@GetMapping(value=AppConstants.GET_BY_NAME_AND_CNUMBER)
	public @ResponseBody AppResponseDto getByNameAndContactNumber(@RequestHeader("name") String name,@RequestHeader("contactNumber") String ContactNUmber){
		AppResponseDto dto = channelServiceImpl.processChannelByNameAndContactNumber(name, ContactNUmber);
	    return dto;
	}
	
	@GetMapping(value=AppConstants.FIND_ALL_CHANNEL_NAMES)
	public AppResponseDto findAllChannelNames() {
		AppResponseDto dto = channelServiceImpl.processFindAllChannelNames();
		return dto;
	}
	
	@PutMapping(value=AppConstants.UPDATE_CONTACTNUMBER_BY_PRIMARY_KEY)
	public AppResponseDto updateContactNumberByPrimaryKey(@PathVariable String contactNumber,@PathVariable long altKey) {
		AppResponseDto dto = channelServiceImpl.processUpdateContactNumberByPrimaryKey(contactNumber, altKey);
	return dto;
	}
	
	@GetMapping(value=AppConstants.GET_ALL_CHANNELS)
	public AppResponseDto getAllChannels() {
		AppResponseDto dto = channelServiceImpl.getChannels();
		return dto;
	}
	
}
