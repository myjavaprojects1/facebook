package com.jsp.facebook.service;

import java.util.List;

import com.jsp.facebook.dto.AppResponseDto;
import com.jsp.facebook.dto.SmsDto;
import com.jsp.facebook.entity.SmsDetails;

public interface ChannelService {

	public AppResponseDto processSms(SmsDto smsDto);
	public AppResponseDto processAllSms(List<SmsDto> smsDto);
	public AppResponseDto processFindSmsById(long id);
	public List<SmsDetails> processChannelBySmsStatus(String smsStatus);
	public AppResponseDto processChannelByNameAndContactNumber(String name,String ContactNumber);
	public AppResponseDto processFindAllChannelNames();
	public AppResponseDto processUpdateContactNumberByPrimaryKey(String contactNumber,long altKey);
	public AppResponseDto getChannels();
	
}
