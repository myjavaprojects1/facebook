package com.jsp.facebook.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsp.facebook.dto.AppResponseDto;
import com.jsp.facebook.dto.SmsDto;
import com.jsp.facebook.entity.SmsDetails;
import com.jsp.facebook.repository.ChannelRepository;

@Service
public class ChannelServiceImpl implements ChannelService{
	
	@Autowired
	private ChannelRepository channelRepository;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public AppResponseDto processSms(SmsDto smsDto) {
		try {
		
		SmsDetails sd = channelRepository.save(createSmsDetails(smsDto));
		if(sd==null) {
			return new AppResponseDto("FAILURE","500" , null, null);
		}
		return new AppResponseDto("SUCCESS", "200", sd, null);
		}
		catch(Exception e){
			return new AppResponseDto("FAILURE", "500", null, e.getLocalizedMessage());
		}
	}

	@Override
	public AppResponseDto processAllSms(List<SmsDto> smsDto) {
		List<SmsDetails> list = new ArrayList<SmsDetails>();
		for (SmsDto sms : smsDto) {
			list.add(createSmsDetails(sms));
		}
		try {
			
			List saveAll = channelRepository.saveAll(list);
			
			if(saveAll.size()==0) {
				return new AppResponseDto("FAILURE", "500", null, null);
			}
			return new AppResponseDto("SUCCESS", "200", saveAll, null);
		}catch(Exception e) {
			return new AppResponseDto("FAILURE", "500", null, e.getLocalizedMessage());
		}
		
	}

	@Override
	public AppResponseDto processFindSmsById(long id) {
		ResponseEntity<String> response = restTemplate.exchange("https://jsonplaceholder.typicode.com/posts", HttpMethod.GET, null, String.class);
		String responseEntity = response.getBody();
		System.out.println(responseEntity);
		try {
			List<List<String>> arrayList = new ArrayList<List<String>>();
			List<Map<String, String>> arrayList2 = new ArrayList<Map<String,String>>();
			
			List<Map<String, String>> list = objectMapper.readValue(responseEntity, new TypeReference<List<Map<String,String>>>(){});
			for (Map<String, String> map : list) {
				List<String> collect=new ArrayList<String>();
				if(map.get("userId").equals("1")) {
					collect = map.keySet().stream().filter(key->key.equals("id") || key.equals("title")).collect(Collectors.toList());
				}
				else continue;
				Map<String, String> hashMap = new HashMap<String,String>();	
			for(String string:collect) {
					hashMap.put(string,map.get(string));
					
				}
			arrayList2.add(hashMap);                                                                
			}
			Map<Integer, List<Map<String,String>>> map = new HashMap<Integer,List<Map<String,String>>>();
			map.put(1,arrayList2);
			System.out.println(map.toString());
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Optional<SmsDetails> optional = channelRepository.findById(id);
		try {
			SmsDetails smsDetails = optional.get();
			if(smsDetails==null) {
				return new AppResponseDto("FAILURE", "500", null, null);
			}

			return new AppResponseDto("SUCCESS", "200", smsDetails, null);
		}
		catch(Exception e) {
			return new AppResponseDto("FAILURE", "500", null, e.getLocalizedMessage());
		}
	
	}

	public SmsDetails createSmsDetails(SmsDto smsDto) {
		SmsDetails smsDetails = new SmsDetails();
		smsDetails.setName(smsDto.getName());
		smsDetails.setContactNumber(smsDto.getContactNumber());
		smsDetails.setSmsContent(smsDto.getSmsContent());
		smsDetails.setCreatedDate(new Date());
		smsDetails.setSmsStatus("SUCCESS");
		
		
		return smsDetails;
		
	}

	@Override
	public List<SmsDetails> processChannelBySmsStatus(String smsStatus) {
//		try {
//		List<SmsDetails> list = channelRepository.getSmsByStatus(smsStatus);
		List<SmsDetails> list = channelRepository.findBySmsStatus(smsStatus);
		return list;
//		if(list.size()==0) {
//			return new AppResponseDto("FAILURE", "500", null, null);
//		}
//		return new AppResponseDto("SUCCESS", "200", list,null);
//		}
//		catch(Exception e){
//			return new AppResponseDto("FAILURE", "500", null, e.getLocalizedMessage());
//		}
	}

	@Override
	public AppResponseDto processChannelByNameAndContactNumber(String name, String ContactNumber) {
		try {
//		SmsDetails smsDetails = channelRepository.getSmsByNameAndContactNumber(name, ContactNumber);
		SmsDetails smsDetails = channelRepository.findByNameAndContactNumber(name, ContactNumber);
		if(smsDetails==null) {
			return new AppResponseDto("FAILURE", "500", null, null);
		}
		return new AppResponseDto("SUCCESS", "200", smsDetails, null);
		}
		catch(Exception e) {
			return new AppResponseDto("FAILURE", "500", null, e.getLocalizedMessage());
		}
	}
	
	@Override
	public AppResponseDto processFindAllChannelNames() {
		try {
			List<String> list = channelRepository.getAllChannelNames();
			if(list.size()==0) {
				return new AppResponseDto("FAILURE", "500", null, null);
			}
			return new AppResponseDto("SUCCESS", "200", list, null);
		}
		catch(Exception e) {
			return new AppResponseDto("FAILURE", "500", null, e.getLocalizedMessage());
		}
	}

	@Override
	public AppResponseDto processUpdateContactNumberByPrimaryKey(String contactNumber,long altKey) {
		try {
		 Integer integer = channelRepository.updateContactNumberByPrimaryKey(contactNumber, altKey);
		if(integer==null) {
			return new AppResponseDto("FAILURE", "500", null, null);
		}
		return new AppResponseDto("SUCCESS", "200", integer, null);
		}catch(Exception e) {
			return new AppResponseDto("FAILURE", "500", null, e.getLocalizedMessage());
		}
	}

	@Override
	public AppResponseDto getChannels() {
		try {
		StringBuilder builder = new StringBuilder();
		builder.append("select * from sms_details");
		List<Map<String, Object>> smsList = jdbcTemplate.queryForList(builder.toString());
		List<SmsDetails> collect = smsList.stream().map(
				(each)->{
					SmsDetails smsDetails = new SmsDetails();
					smsDetails.setAltKey(Long.parseLong(each.get("alt_key").toString()));
					smsDetails.setContactNumber(each.get("contact_number").toString());
					smsDetails.setName(each.get("name").toString());
					smsDetails.setSmsContent(each.get("sms_content").toString());
					smsDetails.setSmsStatus(each.get("sms_status").toString());
//					smsDetails.setCreatedDate((Date)each.get("created_date"));
					return smsDetails;
				}).collect(Collectors.toList());
		if(collect.size()==0) {
			return new AppResponseDto("FAILURE", "500", null, null);
		}
		return new AppResponseDto("SUCCESS", "200", collect, null);
	    }
		catch(Exception e) {
			return new AppResponseDto("FAILURE", "500", null, e.getLocalizedMessage());
		}
	}
}
