package com.jsp.facebook.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="sms_details")
public class SmsDetails implements Serializable {
	
	@Id
	@Column(name="alt_key")
	@GenericGenerator(name="auto-reg",strategy="increment")
	@GeneratedValue(generator="auto-reg")
	private long altKey;
	
	@Column(name="name")
	private String name;
	
	@Column(name="contact_number")
	private String contactNumber;
	
	@Column(name="sms_content")
	private String smsContent;
	
	@Column(name="sms_status")
	private String smsStatus;
	
	@Column(name="created_date")
	private Date createdDate;
	
	
	public long getAltKey() {
		return altKey;
	}

	public void setAltKey(long altKey) {
		this.altKey = altKey;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getSmsContent() {
		return smsContent;
	}

	public void setSmsContent(String smsContent) {
		this.smsContent = smsContent;
	}

	public String getSmsStatus() {
		return smsStatus;
	}

	public void setSmsStatus(String smsStatus) {
		this.smsStatus = smsStatus;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date CreatedDate) {
		this.createdDate = CreatedDate;
	}

	@Override
	public String toString() {
		return "SmsDetails [altKey=" + altKey + ", name=" + name + ", contactNumber=" + contactNumber + ", smsContent="
				+ smsContent + ", smsStatus=" + smsStatus + ", createdDate=" + createdDate + "]";
	}
	
	
	
	
	

}
