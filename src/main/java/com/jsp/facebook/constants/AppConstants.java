package com.jsp.facebook.constants;

public interface AppConstants {

	public static final String SEND_SMS="/sendsms";//post  mapping
	public static final String SEND_ALL_SMS="/sendallsms";//post mapping
	public static final String FIND_SMS_BY_ID="/findsmsbyid/{id}";// get mapping //body contains data for here we provided key inside {}
	public static final String GET_BY_SMS_STATUS="/getchannelbysmsstatus";//get mapping //url straightly contains data as key and value
	public static final String GET_BY_NAME_AND_CNUMBER="/getchannelbynameandcnumber";//get mapping //header contains data as well as some tokens 
	public static final String FIND_ALL_CHANNEL_NAMES="/findallchannelnames";
	public static final String UPDATE_CONTACTNUMBER_BY_PRIMARY_KEY="/updatecontactnumberbyprimarykey/{contactNumber},{altKey}";
	public static final String GET_ALL_CHANNELS="/getallchannels";
}
