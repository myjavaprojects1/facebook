package com.jsp.facebook.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

import com.jsp.facebook.FacebookApplication;
import com.jsp.facebook.entity.SmsDetails;
import com.jsp.facebook.repository.ChannelRepository;
import com.jsp.facebook.service.ChannelServiceImpl;

@RunWith(value=PowerMockRunner.class)
@ContextConfiguration(classes=FacebookApplication.class)
@SpringBootTest(classes=ChannelServiceImplTest.class)
public class ChannelServiceImplTest {

	@MockBean
	private ChannelRepository channelRepository;
	
	@MockBean
	private RestTemplate restTemplate;
	
	@MockBean
	private JdbcTemplate jdbcTemplate;
	
	@InjectMocks
	private ChannelServiceImpl channelServiceImpl;
	
	@Test
	public void testProcessChannelBySmsStatus() throws Exception {
		MockitoAnnotations.openMocks(this);
		
		List<SmsDetails> list = new ArrayList<SmsDetails>();
		
		
		PowerMockito.when(channelRepository,"findBySmsStatus","SUCCESS").thenReturn(list);
		
		System.out.println(list.size());
		
		List<SmsDetails> responseList = channelServiceImpl.processChannelBySmsStatus("SUCCESS");
		
		System.out.println(responseList.size());
		assertEquals(responseList.size(), list.size());
	}
}
