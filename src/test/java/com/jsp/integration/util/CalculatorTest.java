package com.jsp.integration.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
	
	@BeforeAll
	public static void executeBefore() {
		System.out.println("----------------executeBefore-----------------");
	}
	
	@AfterAll
	public static void executeAfter() {
		System.out.println("----------------executeAfter-----------------");
	}
	
	@BeforeEach
	public  void executeBeforeEveryTestCase() {
		System.out.println("----------------executeBeforeEveryTestcases-----------------");
	}
	
	@AfterEach
	public  void executeAfterEveryTestCase() {
		System.out.println("----------------executeAfterEveryTestcases-----------------");
	}
	
	

	@Test
	public void testDivide() {
		Calculator calculator = new Calculator();
		int result = calculator.divide(10, 5);
		assertEquals(result, 2);
		System.out.println("-----------------executeTestDivide-----------------------");
	}
	
	@Test
	public void testDivideWithNumerator() {
		Calculator calculator = new Calculator();
		int result = calculator.divide(-2, 5);
		assertEquals(result,-1 );
		System.out.println("----------------executeTestDivideWithNumerator-----------");
	}
	
	@Test
	public void testDivideWithDenominator() {
		Calculator calculator = new Calculator();
		int result = calculator.divide(5,-2);
		assertEquals(result,-1 );
		System.out.println("----------------executeTestDivideWitDenominator-----------");
	}
}
